addSbtPlugin("com.typesafe.sbt"   % "sbt-scalariform" % "1.3.0")

addSbtPlugin("com.eed3si9n"       % "sbt-assembly"    % "0.14.1")

addSbtPlugin("com.eed3si9n"       % "sbt-buildinfo"   % "0.5.0")

addSbtPlugin("com.timushev.sbt"   % "sbt-updates"     % "0.1.10")

addSbtPlugin("com.typesafe.sbt"   % "sbt-aspectj"     % "0.9.4")

addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.5.0")

