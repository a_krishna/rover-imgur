package com.rover.service.actors

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props, SupervisorStrategy, Terminated}
import com.rover.service.domain._
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable.ListBuffer
import scala.concurrent

case class EventWorkerReference(jobId:String,actorRef: ActorRef)

class FileUploaderManager(repo:JobDataRepo,val workerProps:Props) extends Actor with ActorLogging{

  var currentBuffer:ListBuffer[EventWorkerReference]= ListBuffer()

  override def preStart(): Unit = {
    context.system.eventStream.subscribe(self,classOf[StartProcess])
  }

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(maxNrOfRetries = 3) {
    case _:RuntimeException => Stop
  }

   def receive: Receive = {
     case status:StatusUpdate =>
       println(s"child $status")
       status.data.status match {
         case Waiting =>
         case Downloaded =>repo.get(status.jobId).map { jd =>
           val imgs=jd.imageUrl.map(x => if (x.url == status.data.url) x.copy(status = Downloaded) else x)
           val jStatus=setJobState(imgs,jd.status)
           saveData(jd.copy(imageUrl = imgs,status = jStatus,finished = if(jStatus ==Completed) Some(DateTime.now(DateTimeZone.UTC).getMillis) else None))
         }
         case Uploaded =>
           repo.get(status.jobId).map { jd =>
             val imgs=jd.imageUrl.map(x=> if(x.url == status.data.url ) x.copy(status=Uploaded,responseData = status.data.responseData) else x )
             val jStatus=setJobState(imgs,jd.status)
             saveData(jd.copy(imageUrl = imgs,status = jStatus,finished = if(jStatus ==Completed) Some(DateTime.now(DateTimeZone.UTC).getMillis) else None))
           }
         case UploadFailed =>
           repo.get(status.jobId).map { jd =>
             val imgs=jd.imageUrl.map(x => if (x.url == status.data.url) x.copy(status = UploadFailed, responseData = status.data.responseData) else x)
             val jStatus=setJobState(imgs,jd.status)
             saveData(jd.copy(imageUrl = imgs,status = jStatus,finished = if(jStatus ==Completed) Some(DateTime.now(DateTimeZone.UTC).getMillis) else None))
           }
         case DownloadFailed =>
           repo.get(status.jobId).map { jd =>
             val imgs=jd.imageUrl.map(x => if (x.url == status.data.url) x.copy(status = DownloadFailed) else x)
             val jStatus=setJobState(imgs,jd.status)
             saveData(jd.copy(imageUrl = imgs,status = jStatus,finished = if(jStatus ==Completed) Some(DateTime.now(DateTimeZone.UTC).getMillis) else None))
           }
       }
     case m @ Terminated(child) =>currentBuffer= currentBuffer filterNot(_.actorRef == child)

     case st:StartProcess =>
       concurrent.blocking {
         repo.insert(st.jobData)
         createActor(st.jobData)
       }

     case _ =>
   }

  def saveData(jobData: JobData):Unit={
    repo.update(jobData.id,jobData)
  }

  def setJobState(urls:Seq[ImageData], existingStatus:JobStatus):JobStatus=
    existingStatus match {
      case InProgress => if(urls.filter(_.status == Waiting).length == 0) Completed else  existingStatus
      case _ => if (urls.filter(_.status == Waiting).length != 0) InProgress else existingStatus
  }

  def createActor(jobData: JobData)={
    log.info(s"Creating Child Actor ${jobData.id}")
    val actorRef=context.actorOf(workerProps,name = s"job-${jobData.id}")
    currentBuffer += EventWorkerReference(jobData.id,actorRef)
    actorRef ! ProcessFile(jobData.id,jobData.imageUrl.map(_.url))
  }

}
