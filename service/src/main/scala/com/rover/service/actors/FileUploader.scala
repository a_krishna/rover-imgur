package com.rover.service.actors

import java.io._
import java.net.{HttpURLConnection, URL}
import java.nio.file.attribute.FileAttribute
import java.nio.file.{Files, Paths}

import akka.actor.{Actor, ActorLogging}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.io.SynchronousFileSource
import akka.stream.scaladsl.Source
import com.rover.service.Settings
import com.rover.service.domain._

import scala.concurrent.Future
import scala.util.{Failure, Success}

class FileUploader(endPointUrl:String) extends Actor with ActorLogging{

  import context.dispatcher
  implicit val materialzer = ActorMaterializer()
  import context.system


  def createEntity(file: File): Future[RequestEntity] = {
    require(file.exists())
    val formData =
      Multipart.FormData(
        Source.single(
          Multipart.FormData.BodyPart(
            "image",
            HttpEntity(MediaTypes.`application/octet-stream`, file.length(),SynchronousFileSource(file,100000)), // the chunk size here is currently critical for performance
            Map.empty[String,String])))
    Marshal(formData).to[RequestEntity]
  }

  def createRequest(file: File,accessToken:String):Future[HttpRequest]=for{
    e <- createEntity(file)
  }yield HttpRequest(HttpMethods.POST,uri = endPointUrl,entity = e,headers = List(headers.RawHeader("Authorization",s"Bearer ${accessToken}")) )

  def processImage(filePath:String):Future[String]=for{
    req <- createRequest(new File(filePath),Token.token.map(_.access_token.getOrElse(Settings.clientId)).getOrElse("") )
    res <- Http().singleRequest(req)
    responseBodyAsString ← Unmarshal(res).to[String]
  } yield responseBodyAsString


  def downloadImage(jobId:String,url:String)= {
    val folderPath=Paths.get(s"${Settings.imageStorePath}/$jobId/")
    if(!Files.exists(folderPath)){
      import java.nio.file.attribute.PosixFilePermissions
      val permissions = PosixFilePermissions.fromString("rwxrwxrwx")
      val fileAttributes = PosixFilePermissions.asFileAttribute(permissions)
      Files.createDirectory(folderPath,fileAttributes)
    }
    val fileToDownloadAs = new File(s"${Settings.imageStorePath}/$jobId/${url.split("/").last}")
    val out: OutputStream = new BufferedOutputStream(new FileOutputStream(fileToDownloadAs))
    try {
      val src = new URL(url)
      val conn = src.openConnection().asInstanceOf[HttpURLConnection]
      conn.setRequestMethod("GET")
      val in = conn.getInputStream
      val byteArray = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
      out.write(byteArray)
      out.flush()
      out.close()
      self ! StatusUpdate(jobId, ImageData(url = url, status = Downloaded))
    } catch {
      case exe: FileNotFoundException => self ! StatusUpdate(jobId, ImageData(url = url, status = DownloadFailed))
    }
    finally {
      out.flush()
      out.close()
    }
  }

  import com.rover.service.domain.PostResponse._

  def receive ={
    case pv:ProcessFile =>
      println("RECV Update",pv)
      pv.filePath.map { url =>
       downloadImage(pv.jobId,url)
      }
    case status:StatusUpdate =>
      println(s"child $status")
      context.parent ! status
      status.data.status match {
        case Waiting =>
        case DownloadFailed =>
        case Downloaded => processImage(s"${Settings.imageStorePath}/${status.jobId}/${status.data.url.split("/").last}").onComplete(z => z match {
      case Success(x) => self ! StatusUpdate(status.jobId, ImageData(url=status.data.url,responseData = convertResponse(x),status = getStatusOfUploadResponse(convertResponse(x))))
      case Failure(exception) => self ! StatusUpdate(status.jobId, ImageData(url=status.data.url,responseData = None,status = UploadFailed))
      })
        case UploadFailed =>
        case Uploaded =>
      }
    case _ =>
  }
}
