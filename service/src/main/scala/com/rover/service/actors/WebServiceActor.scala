package com.rover.service.actors


import java.io.{BufferedInputStream, FileInputStream}

import akka.actor.{ActorLogging, Props}
import akka.io.IO
import akka.stream.ActorMaterializer
import com.rover.service.Settings.{clientId, imageUrl}
import com.rover.service.domain.{ImageEvent, JobDataRepo, ProcessFile}
import com.rover.service.routes._
import com.rover.service.{ExternalWebService, Settings}
import spray.can.Http
import spray.routing.HttpServiceActor

class WebServiceActor(host: String, port: Int,eventBus:EventService[ImageEvent],jobDataRepo: JobDataRepo) extends HttpServiceActor
  with ActorLogging
  with CORSDirectives
  with ImgurRoute
  with OAuthRoute {

  import context.dispatcher //Used mainly by routes that need thread pools for Futures/asks
  implicit val materlizer=ActorMaterializer()

  //Start this actor as a web server
  IO(Http)(context.system) ! Http.Bind(listener = self, interface = host, port = port)

  implicit val webServiceTimeout = Settings.webServiceTimeout

  implicit val system = context.system
  val externalWebService=new ExternalWebService(Settings.endpointURL)

//  eventBus.publish(ProcessFile("/home/aravind/Documents/workspace/rover-imgur/service/src/main/resources/11234651086_681b3c2c00_b.jpg",clientId))

//  val img = new BufferedInputStream(new FileInputStream("/home/aravind/Documents/workspace/rover-imgur/service/src/main/resources/11234651086_681b3c2c00_b.jpg"))
//  val bArray = Stream.continually(img.read()).takeWhile(-1 !=).map(_.toByte).toArray
//  externalWebService.imageUploader(imageUrl, clientId, bArray)
  //  val sessionAuthenticator = SessionService.sessionAuthenticator(system)

  //Aggregate and run all the routes
  def receive = runRoute(
    cors(EchoAllOriginsAccepted)(
      imgurRoute(eventBus,externalWebService,jobDataRepo) ~
      oauthRoute(jobDataRepo)
    ))

}