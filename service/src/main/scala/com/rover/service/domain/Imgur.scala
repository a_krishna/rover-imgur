package com.rover.service.domain

import java.text.SimpleDateFormat
import java.util.UUID

import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsObject, Json}
trait JobStatus
case object Pending extends JobStatus
case object InProgress extends JobStatus
case object Completed extends JobStatus

object JobStatus{
  def convert(d:JobStatus):String= d match {
    case Pending => "pending"
    case InProgress =>"in-progress"
    case Completed => "completed"
  }
}

trait ImageStatus
trait DownloadStatus extends ImageStatus
trait UploadStatus extends ImageStatus
case object Waiting extends UploadStatus
case object Uploaded extends UploadStatus
case object UploadFailed extends UploadStatus
case object Downloaded extends DownloadStatus
case object DownloadFailed extends DownloadStatus

case class Token(state:String,access_token:Option[String],token_type:Option[String],refresh_token:Option[String])

object Token{
  var clientId:Option[String]=None
  var clientSecret:Option[String]=None
  var token:Option[Token]=None
}
case class JobData(
     id:String=UUID.randomUUID().toString,
     created:Long=DateTime.now(DateTimeZone.UTC).getMillis,
     finished:Option[Long]=None,
     imageUrl:Seq[ImageData],
     status:JobStatus=Pending
     )


object JobData{

  import JobStatus._
  import Timeseries._

  def getLinks(datas:Seq[JobData]):JsObject={
   val records= datas.foldLeft(Seq.empty[String]){(z,a)=> a.status match {
     case Completed => z ++ a.imageUrl.map(x=> x.status match {
       case Uploaded => x.responseData.map(_.data  match {
         case dv:ImgDetails => dv.link
         case er:ErrorDetails =>  ""
       }).getOrElse("")
       case _ =>  ""
     })
     case _ => z
   }}
   Json.obj("uploaded"->records)
  }


  def getJobDetails(data:JobData):JsObject={
    val uploaded=data.imageUrl.foldLeft(Map("pending"->Set.empty[String],"complete"->Set.empty[String],"failed"->Set.empty[String]) ){(z,a)=>
      a.status match {
        case Waiting =>  z++ z.get("pending").map(x=>Map("pending" -> x.+(a.url))).getOrElse(Map("pending" -> Set(a.url)))
        case Uploaded =>z ++ z.get("complete").map(x=>Map("complete" -> x.+(a.responseData.map(_.data.asInstanceOf[ImgDetails].link).getOrElse(a.url) ))).getOrElse(Map("complete" -> Set(a.responseData.map(_.data.asInstanceOf[ImgDetails].link).getOrElse(a.url))))
        case UploadFailed => z ++ z.get("failed").map(x=>Map("failed" -> x.+(a.url))).getOrElse(Map("failed" -> Set(a.url)))
        case DownloadFailed =>z ++ z.get("failed").map(x=>Map("failed" -> x.+(a.url))).getOrElse(Map("failed" -> Set(a.url)))
        case Downloaded =>z ++ z.get("pending").map(x=>Map("pending" -> x.+(a.url))).getOrElse(Map("pending" -> Set(a.url)))
        case _ => z
      }
    }
   Json.obj("id"->data.id,"created"->humanReadableTime(data.created),"finished"->data.finished.map(humanReadableTime(_)),"status"->convert(data.status),"uploaded"->uploaded)
  }

}


object Timeseries{
  val format= new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss.SSSZ")

  def humanReadableTime(millis:Long):String=format.format(millis)
}
case class ImageData(url:String,responseData:Option[PostResponse]=None,status: ImageStatus=Waiting)

sealed trait ImageEvent
case class ImgurClient(clientId:String,clientSecret:String)

case class ProcessFile(jobId:String,filePath:Seq[String]) extends ImageEvent
case class StatusUpdate(jobId:String,data:ImageData) extends ImageEvent

case class UploadImages(urls:Seq[String])

case class StartProcess(jobData: JobData) extends ImageEvent

object UploadImages{
  implicit  val reads= Json.reads[UploadImages]
}


trait DataDetails
case class ImgDetails(
     id:String,
     title:Option[String],
     description:Option[String],
     dateTime:Long,
     animated:Boolean,
     width:Int,
     height:Int,
     size:Int,
     views:Int,
     bandwidth:Int,
     vote:Option[String],
     favorite:Boolean,
     account_url:Option[String],
     account_id:Int,
     is_ad:Boolean,
     in_most_viral:Boolean,
     ad_url:String,
     in_gallery:Boolean,
     deletehash:String,
     name:String,
     link:String) extends DataDetails
case class ErrorDetails(error:String,request:String,method:String) extends DataDetails

class Data[A](val data:A)

case class PostResponse(data:DataDetails,success:Boolean,status:Int)


import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

object PostResponse{
  val errorRead:Reads[DataDetails]=(

    (JsPath \ "method").read[String] and
      (JsPath \"error").read[String] and
      (JsPath \"request").read[String]
    )(ErrorDetails.apply _)

  val successRead:Reads[DataDetails]=(
    (JsPath \"id").read[String] and
      (JsPath \"title").read[Option[String]] and
      (JsPath \"description").read[Option[String]] and
      (JsPath \"datetime").read[Long] and
      (JsPath \"animated").read[Boolean] and
      (JsPath \"width").read[Int] and
      (JsPath \"height").read[Int] and
      (JsPath \"size").read[Int] and
      (JsPath \"views").read[Int] and
      (JsPath \"bandwidth").read[Int] and
      (JsPath \"vote").read[Option[String]] and
      (JsPath \"favorite").read[Boolean] and
      (JsPath \"account_url").read[Option[String]] and
      (JsPath \"account_id").read[Int] and
      (JsPath \"is_ad").read[Boolean] and
      (JsPath \ "in_most_viral").read[Boolean] and
      (JsPath \ "ad_url").read[String] and
      (JsPath \ "in_gallery").read[Boolean] and
      (JsPath \ "deletehash").read[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "link").read[String]

    )(ImgDetails.apply _)

  implicit  val postReads:Reads[PostResponse]= (
    (JsPath \ "data").read[DataDetails](successRead) or
      (JsPath \ "data").read[DataDetails](errorRead) and
      (JsPath \ "success").read[Boolean] and
      (JsPath \ "status").read[Int]


    )(PostResponse.apply _)

  def convertResponse(x:String)={
    val data =Json.parse(x)
    data.asOpt[PostResponse]
  }

  def getStatusOfUploadResponse(response:Option[PostResponse])= response match {
    case Some(x) if x.status == 200 => Uploaded
    case Some(x) => UploadFailed
    case None  => UploadFailed
  }
}


