package com.rover.service.domain

import java.util.UUID

import scala.collection.immutable.Seq
import scala.concurrent.Future
import scala.concurrent.stm.TMap

sealed trait UpsertStatus
object UpsertStatus {
  case object Updated extends UpsertStatus
  case object Inserted extends UpsertStatus
}

trait Repository {
  type Id <: AnyRef //Entity's unique identifier (e.g. UUID). May be a tuple or case class in the case of compound primary keys
  type Entity <: AnyRef //Business domain object with a unique identifier
  type PartialEntity <: AnyRef //Container of entity fields that need updating. Usually a case class mimicking the entity but with all optional fields

  def id(entity: Entity): Id //Extracts entity's id for usage in queries
  def entity(partialEntity: PartialEntity):Entity
  def get(id: Id): Option[Entity]
  def get(ids: Set[Id]): Seq[Entity]
  def getAll: Seq[Entity]
  def insert(entity: Entity): Unit
  def insert(entities: Seq[Entity]): Unit
  def delete(id: Id): Unit
  def update(id: Id, partialEntity: PartialEntity): Unit
  def upsert(entity: Entity): UpsertStatus

}

trait InMemoryRepository extends Repository {

  protected val map = TMap.empty[Id, Entity]

  def get(id: Id): Option[Entity] = map.single.get(id)
  def getAll: Seq[Entity] = map.single.values.toList
  def insert(entity: Entity): Unit = map.single.update(id(entity), entity)
  def insert(entities: Seq[Entity]): Unit = map.single ++= entities.map(entity ⇒ (id(entity), entity))
  def delete(id: Id): Unit = map.single.remove(id)
  def update(id: Id, partial: PartialEntity): Unit
  def upsert(entity: Entity): UpsertStatus

}

trait JobDataRepo extends Repository{
  type Id = String
  type Entity = JobData
  type PartialEntity = JobData

  def id(entity: Entity): Id = entity.id

  def entity(partialEntity: PartialEntity): Entity = partialEntity
  def newId: Id = UUID.randomUUID().toString

  def checkUrlsExists(urls:Set[String]):Future[Boolean]
}

class InMemoryJobDataEntityRepo()extends JobDataRepo with InMemoryRepository{

  override def get(ids: Set[String]): Seq[JobData] = getAll.filter(z=> ids.contains(z.id))

  override def update(id: String, partial: JobData): Unit = map.single.update(id,partial)

  override def upsert(entity: JobData): UpsertStatus = get(entity.id) match {
    case Some(x) => update(entity.id,entity)
      UpsertStatus.Updated
    case None =>insert(entity)
      UpsertStatus.Inserted
  }

  override def checkUrlsExists(urls: Set[String]): Future[Boolean] = Future.successful(getAll.map(_.imageUrl.map(_.url)).flatten.toSet.intersect(urls).size == 0)
}
