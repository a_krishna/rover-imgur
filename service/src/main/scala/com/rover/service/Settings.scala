package com.rover.service

import java.net.InetAddress
import java.util.UUID

import akka.util.Timeout
import com.typesafe.config.{ Config, ConfigFactory }

class Settings(val config: Config) {

  val myimageConfig = config.getConfig("rover")
  import myimageConfig._

  val host = if (hasPath("host")) getString("host") else InetAddress.getLoopbackAddress.getHostAddress
  val port = getInt("port")
  val webServiceTimeout = Timeout(getFiniteDuration("web-service-timeout"))
  val endpointURL= getString("app-url")
  val clientId= getString("client-id")
  val clientSecret= getString("client-secret")
  val oauthAuthorize=getString("oauth-url")
  val imageUrl=getString("image-url")
  val imageStorePath=getString("image-store-path")


  //Helper methods
  def getFiniteDuration(path: String) = {
    import scala.concurrent.duration._
    getDuration(path, java.util.concurrent.TimeUnit.MILLISECONDS).millis
  }

  def getCfgList(path: String) = {
    import scala.collection.JavaConverters._
    getConfigList(path).asScala.toList
  }

}

object Settings extends Settings(ConfigFactory.load)

