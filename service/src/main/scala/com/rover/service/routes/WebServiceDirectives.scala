package com.rover.service.routes

import java.net.URL

import akka.actor.ActorRefFactory
import com.typesafe.scalalogging.LazyLogging
import spray.client.pipelining._
import spray.http.StatusCodes
import spray.httpx.unmarshalling.FromResponseUnmarshaller
import spray.routing.Directive1
import spray.routing.Directives._

import scala.concurrent.{ Future, ExecutionContext }

trait WebserviceDirectives {
  def webserviceEntity[T](magnet: WebserviceMagnet[T]): Directive1[T] = magnet()
}
class WebserviceMagnet[T: FromResponseUnmarshaller](url: URL)(implicit ec: ExecutionContext, actorRef: ActorRefFactory)
  extends LazyLogging {
  def apply(): Directive1[T] = {
    onSuccess(webservice(url)).flatMap {
      case Some(entity) ⇒ provide(entity)
      case None ⇒
        logger.warn(s"Empty response from $url")
        complete(StatusCodes.NotFound)
    }
  }

  def webservice(url: URL): Future[Option[T]] = {
    val pipeline = sendReceive ~> unmarshal[Option[T]]
    pipeline(Get(url.toString))
  }
}

object WebserviceMagnet {
  implicit def fromWs[T: FromResponseUnmarshaller](url: URL)(implicit ec: ExecutionContext, actorRef: ActorRefFactory): WebserviceMagnet[T] =
    new WebserviceMagnet(url)
}
