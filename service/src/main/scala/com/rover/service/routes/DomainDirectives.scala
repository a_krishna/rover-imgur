package com.rover.service.routes

import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directive1
import spray.routing.Directives.{complete, onSuccess, provide, validate}

import scala.concurrent.{ExecutionContext, Future}

trait DomainDirectives {

  import DomainDirectives._

  def asyncFind[T](magnet: AsyncFindMagnet[T]): Directive1[T] = magnet()

  def requireOption[T](option: Option[T], errorMsg: String): Directive1[T] =
    validate(option.isDefined, errorMsg).hflatMap(_ ⇒ provide(option.get))

}

object DomainDirectives extends PlayJsonSupport {


  implicit class AsyncFindMagnet[T](finder: ⇒ Option[T])(implicit ec: ExecutionContext) {
    def apply(): Directive1[T] =
      onSuccess(Future(finder)).flatMap {
        case Some(entity) ⇒ provide(entity)
        case None         ⇒ complete(StatusCodes.NotFound)
      }
  }

}
