package com.rover.service.routes

import com.rover.service.domain.{JobDataRepo, Token}
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directives
import spray.util.LoggingContext

import scala.concurrent.ExecutionContext

trait OAuthRoute extends Directives with PlayJsonSupport with DomainDirectives with WebserviceDirectives {
  def oauthRoute(jobDataRepo: JobDataRepo)(implicit executionContext: ExecutionContext, loggingContext: LoggingContext) ={
    (pathPrefix("oauth")){
        pathPrefix("callback"){
          get{
            parameters('state,'access_token?, 'token_type?,'refresh_token?){(s,a,t,r)=>
              Token.token=Some(Token(s,a,t,r))
              complete(StatusCodes.OK,Token(s,a,t,r).toString)
            }
          }
        } ~pathPrefix("user"/"update"){
          get{
            parameters('clientId,'clientSecret?){(c,s)=>
              Token.clientId=Some(c)
              Token.clientSecret=s
              complete(StatusCodes.OK,s" $c $s")
            }
          }
        }
    }
  }
}
