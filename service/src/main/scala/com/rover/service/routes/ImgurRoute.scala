package com.rover.service.routes

import com.rover.service.actors.EventService
import com.rover.service.domain._
import com.rover.service.{Settings, WebCallDAO}
import play.api.libs.json.Json
import spray.http.MediaTypes._
import spray.http.{HttpEntity, HttpResponse, StatusCodes}
import spray.httpx.PlayJsonSupport
import spray.routing.Directives
import spray.util.LoggingContext

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait ImgurRoute extends Directives with PlayJsonSupport with DomainDirectives with WebserviceDirectives {

  import JobData._
  import com.rover.service.Settings._
  def imgurRoute(eventService: EventService[ImageEvent],externalWebService: WebCallDAO,jobDataRepo: JobDataRepo)(implicit executionContext: ExecutionContext, loggingContext: LoggingContext) =
    (pathPrefix("v1")){
      pathEndOrSingleSlash{
        get{
          complete{
           Future{
             statsPresentation(s"${Settings.endpointURL}${oauthAuthorize}?client_id=${Token.clientId.getOrElse(clientId)}&response_type=token&state=RoverConnectOne")
           }
          }

        }
      } ~ pathPrefix("upload"/Segment){name=>
        get {
          complete {
            Future {
              eventService.publish(ProcessFile("",Seq(s"/home/aravind/Documents/workspace/rover-imgur/service/src/main/resources/$name")))
              "Uploaded"
            }
            }
        }
      } ~ pathPrefix("images"){
         pathEndOrSingleSlash{
           get{
             complete(getLinks(jobDataRepo.getAll))
           }
         } ~ pathPrefix("upload"/Segment.?){ jobId=>
            pathEndOrSingleSlash{
              post{
                entity(as[UploadImages]) { uploadImages =>
                  onComplete(jobDataRepo.checkUrlsExists(uploadImages.urls.toSet)) {
                    _ match {
                      case Success(value) =>
                        if(value) {
                          val jd = JobData(imageUrl = uploadImages.urls.map(z => ImageData(url = z, status = Waiting)))
                          eventService.publish(StartProcess(jd))
                          complete(StatusCodes.OK, Json.obj("job-id" -> jd.id))
                        }
                        else {
                          complete(StatusCodes.BadRequest,Json.obj("error" ->"Duplicates Urls Found"))
                        }
                      case Failure(exception) =>
                        complete(StatusCodes.InternalServerError)
                    }

                  }
                }
              } ~ get{
                 jobId match {
                   case Some(jid) => asyncFind(jobDataRepo.get(jid)){ jd =>
                     complete(StatusCodes.OK,getJobDetails(jd))
                   }
                   case None =>complete(StatusCodes.BadRequest,"No Match Found")
                 }
              }
            }

         }
      }
    }

  def statsPresentation(sso:String) = HttpResponse(
    entity = HttpEntity(`text/html`,
      <html>
        <body>
          <a href={sso}>SingIn</a>
        </body>
      </html>.toString()
    )
  )

}
