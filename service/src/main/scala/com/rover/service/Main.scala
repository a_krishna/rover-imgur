package com.rover.service

import akka.actor.{ActorSystem, Props}
import com.rover.service.actors.{AkkaEventService, FileUploader, FileUploaderManager, WebServiceActor}
import com.rover.service.domain.{ImageEvent, InMemoryJobDataEntityRepo}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object Main extends App with StrictLogging{
  implicit val actorSystem = ActorSystem("rover")
  import actorSystem.dispatcher

  val workers:String => Props ={url=> Props(new FileUploader(url))}
  val jobDataRepo=new InMemoryJobDataEntityRepo()
  actorSystem.actorOf(Props(new FileUploaderManager(jobDataRepo,workers(Settings.endpointURL+Settings.imageUrl))),"file-uploader-manager")
  actorSystem.actorOf(Props(new WebServiceActor(Settings.host, Settings.port,new AkkaEventService[ImageEvent],jobDataRepo)), name = "web-service")

  sys.addShutdownHook {
    actorSystem.log.info("Shutting down")
    actorSystem.shutdown()
    actorSystem.awaitTermination()
    logger.info(s"Actor system '${actorSystem.name}' successfully shut down")
  }
  println("Hello Rover")
}