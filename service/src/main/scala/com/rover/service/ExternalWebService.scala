package com.rover.service

import akka.actor.ActorSystem
import spray.http.{HttpRequest, HttpResponse}
import spray.client.pipelining._
import spray.http._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait  WebCallDAO {

  def authorizeUser(url:String):Future[HttpResponse]
  def imageUploader(url:String,clientId:String,img:Array[Byte]):Unit
}
class ExternalWebService(endPointUrl:String)(implicit system: ActorSystem, ec: ExecutionContext) extends WebCallDAO {

  val log = akka.event.Logging(system, this.getClass)
  val logRequest: HttpRequest ⇒ HttpRequest = { r ⇒ log.debug(s"External IMGUR Request: $r"); r }
  val logResponse: HttpResponse ⇒ HttpResponse = { r ⇒ log.debug(s"IMGUR Response: ${r.toString}..."); r }
  protected[this] lazy val pipeline: HttpRequest ⇒ Future[HttpResponse] = (
    logRequest
      ~> sendReceive
      ~> logResponse)

  def authorizeUser(url:String):Future[HttpResponse]={
    pipeline(Get(endPointUrl+url))
  }

  def imageUploader(url:String,clientId:String,img:Array[Byte])={
    val httpData=HttpData(img)
    val formData=FormData(Map("image"-> httpData.asString))
    pipeline(Post(endPointUrl+url,formData).withHeaders(HttpHeaders.RawHeader.apply("Authorization",s"Client-ID $clientId")) ).onComplete(z=> z match {
      case Success(x) => println(x.status,"  ",x.entity,"  ",x.message)
      case Failure(err) =>println(err)
    }
    )
  }
}
