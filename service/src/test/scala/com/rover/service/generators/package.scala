package com.rover.service

import com.rover.service.domain.{ErrorDetails, ImageData, ImgDetails, JobData}
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary

package object generators {

  import org.scalacheck.Gen._

  object imgDetails{
    implicit val genImgDetails=Arbitrary(for{
      id <- alphaStr
      title <- option(alphaStr)
      description<- option(alphaStr)
      dateTime<- posNum[Long]
      animated<- arbitrary[Boolean]
      width <- posNum[Int]
      height<- posNum[Int]
      size<- posNum[Int]
      views<- posNum[Int]
      bandwidth<- posNum[Int]
      vote<- option(alphaStr)
      favorite<- arbitrary[Boolean]
      account_url<- option(alphaStr)
      account_id<- posNum[Int]
      is_ad<- arbitrary[Boolean]
      in_most_viral<- arbitrary[Boolean]
      ad_url <- alphaStr
      in_gallery<- arbitrary[Boolean]
      deletehash <- alphaStr
      name <- alphaStr
      link <- alphaStr
    }yield {
      ImgDetails(id,title,description,dateTime,animated,width,height,size,views,bandwidth,vote,favorite,account_url,account_id,is_ad,in_most_viral,ad_url,in_gallery,deletehash,name,link)
    })

    def next()=Arbitrary.arbitrary[ImgDetails].sample.get
  }

  object errorDetails{
    implicit val genErrorDetails=Arbitrary(for{
      error <- alphaStr
      request <- alphaStr
      method <- alphaStr
    }yield {
      ErrorDetails(error,request,method)
    })

    def next()=Arbitrary.arbitrary[ErrorDetails].sample.get
  }

  object jobdata{

    implicit  val genJobData=Arbitrary(for{
      id <- alphaStr
      created <- posNum[Long]
      finished <- option(posNum[Long])
    }yield {
      JobData(id,created,finished,Seq.empty[ImageData])
    })

    def next()=Arbitrary.arbitrary[JobData].sample.get
  }
}
