package com.rover.service.domain

import com.rover.service.util.TestImgurData
import org.scalatest._
import play.api.libs.json.{JsPath, JsValue, Json}
import spray.httpx.PlayJsonSupport

case object TestData extends TestImgurData with PlayJsonSupport

class ImgurDomainSpec extends FlatSpec with MustMatchers {

  val testData=TestData
   behavior of "Imgur Data Convertion "

    it must "Return Success Image Links" in {
      JobData.getLinks(testData.jobs) mustBe Json.obj("uploaded" -> Seq("https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","","","","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg"))
     }

    it must "Return Job Details with Waiting Status"  in{
      val json:JsValue=JobData.getJobDetails(testData.jobData)
      (json \ "status").as[String] mustBe "pending"
      val uploaded=(json \ "uploaded").as[Map[String,Set[String]]]
      uploaded.get("pending").map(_.size) mustBe Some(3)
      uploaded.get("complete").map(_.size) mustBe Some(0)
      uploaded.get("failed").map(_.size) mustBe Some(0)
    }

    it must "Return Job Details with Complete and Failed" in{
      val json:JsValue=JobData.getJobDetails(testData.jobData1)
      (json \ "status").as[String] mustBe "completed"
      val uploaded=(json \ "uploaded").as[Map[String,Set[String]]]
      uploaded.get("pending").map(_.size) mustBe Some(0)
      uploaded.get("complete").map(_.size) mustBe Some(1)
      uploaded.get("failed").map(_.size) mustBe Some(3)
    }

    it must "Return Job Details with Completed links" in {
      val json:JsValue=JobData.getJobDetails(testData.jobData2)
      (json \ "status").as[String] mustBe "completed"
      val uploaded=(json \ "uploaded").as[Map[String,Set[String]]]
      uploaded.get("pending").map(_.size) mustBe Some(0)
      uploaded.get("complete").map(_.size) mustBe Some(1)
      uploaded.get("failed").map(_.size) mustBe Some(0)
    }

   it must "Return Job Details with InProgress state" in {
    val json:JsValue=JobData.getJobDetails(testData.jobData4)
    (json \ "status").as[String] mustBe "in-progress"
    val uploaded=(json \ "uploaded").as[Map[String,Set[String]]]
    uploaded.get("pending").map(_.size) mustBe Some(3)
    uploaded.get("complete").map(_.size) mustBe Some(1)
    uploaded.get("failed").map(_.size) mustBe Some(0)
  }
}
