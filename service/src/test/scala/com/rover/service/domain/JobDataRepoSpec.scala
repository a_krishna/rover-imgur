package com.rover.service.domain

import com.rover.service.domain.UpsertStatus.{Inserted, Updated}
import com.rover.service.util.TestImgurData
import org.scalatest.{FlatSpec, Matchers}
import com.rover.service.generators._
class JobDataRepoSpec extends FlatSpec with Matchers{


  "JobData" must "insert into repo" in new RepoContext {
   repo.insert(jobData)
   repo.insert(jobData1)
   repo.insert(jobData2)
   repo.insert(jobData3)

    repo.get(jobData.id).map(_.status) shouldBe(Some(Pending))
    repo.getAll.size shouldBe 4
  }

  "JobData" must "insert and update into repo" in new RepoContext{

    repo.insert(jobData)
    repo.insert(jobData1)
    repo.insert(jobData2)
    repo.insert(jobData3)

    repo.get(jobData1.id).map(_.status) shouldBe(Some(Completed))
    repo.update(jobData2.id,jobData)
    repo.get(jobData2.id) shouldBe Some(jobData)
  }

  "JobData" must "insert and upsert " in new RepoContext {
    repo.insert(jobData)
    repo.insert(jobData1)
    repo.get(jobData1.id).map(_.imageUrl) shouldBe(Some(jobData1.imageUrl))
    repo.upsert(jobData3) shouldBe Inserted
    repo.get(jobData3.id).map(_.status) shouldBe Some(InProgress)
    repo.upsert(jobData3.copy(status = Completed)) shouldBe Updated
    repo.get(jobData3.id).map(_.status) shouldBe Some(Completed)
  }

  "JobData" must "insert update and delete" in new RepoContext {
    repo.insert(jobData)
    repo.insert(jobData1)

    repo.getAll.size shouldBe 2

    repo.delete(jobData1.id)

    repo.getAll.size shouldBe 1

    repo.delete(jobData2.id)

    repo.getAll.size shouldBe 1

    val details=(0 to 4).toSeq.map(z=> ImageData("http://123",Some(PostResponse(imgDetails.genImgDetails.arbitrary.sample.get,true,200)),Uploaded))
    val datas=(0 to 4).toSeq.map(z=> jobdata.genJobData.arbitrary.sample.get.copy(imageUrl = details.toSeq))
    repo.insert(datas)

    repo.getAll.size shouldBe 6

  }

  trait RepoContext extends TestImgurData{
    val repo=new InMemoryJobDataEntityRepo()
  }

}
