package com.rover.service.util

import com.rover.service.domain._
import play.api.libs.json.Json

trait TestImgurData {


  val errorMsg=PostResponse(ErrorDetails("POST","No image data was sent to the upload api","/3/image"),false,400)
  val successMsg=PostResponse(ImgDetails("ucmiIDt",None,None,1546068918,false,805,1024,352500,0,0,None,false,None,99426657,false,false,"",false,"1RlVHiKJloHR7gA","","https://i.imgur.com/ucmiIDt.jpg"),true,200)
  val waitingUrls=Seq(ImageData(url = "http://link-1.jpg"),ImageData(url = "http://link-2.jpg"),ImageData(url = "http://link-3.jpg"))
  val downloadFailedUrls=Seq(ImageData(url = "http://link-1.jpg",status =DownloadFailed ),ImageData(url = "http://link-2.jpg",status =DownloadFailed),ImageData(url = "http://link-3.jpg",status =DownloadFailed))
  val uploadedUrls=Seq(ImageData(url = "http://link-1.jpg",responseData = Some(successMsg) ,status =Uploaded),ImageData(url = "http://link-2.jpg",responseData = Some(successMsg),status =Uploaded),ImageData(url = "http://link-3.jpg",responseData = Some(successMsg),status =Uploaded))
  val uploadedFailedUrls=Seq(ImageData(url = "http://link-1.jpg",responseData = Some(errorMsg),status =UploadFailed),ImageData(url = "http://link-2.jpg",responseData = Some(errorMsg),status =UploadFailed),ImageData(url = "http://link-3.jpg",responseData = Some(errorMsg),status =UploadFailed))
  val downloadedUrls=Seq(ImageData(url = "http://link-1.jpg",status =Downloaded),ImageData(url = "http://link-2.jpg",status =Downloaded),ImageData(url = "http://link-3.jpg",status =Downloaded))
  val jobData=JobData(imageUrl = waitingUrls)
  val jobData1=JobData(imageUrl = uploadedUrls ++ uploadedFailedUrls,status = Completed)
  val jobData2=JobData(imageUrl = uploadedUrls,status = Completed)
  val jobData3=JobData(imageUrl = downloadFailedUrls++uploadedUrls,status = InProgress)
  val jobData4=JobData(imageUrl = downloadedUrls++waitingUrls++uploadedUrls,status =InProgress )

  val jobs=Seq(jobData,jobData1,jobData2,jobData3,jobData4)

  val msg="""{"data":{"error":"No image data was sent to the upload api","request":"\/3\/image","method":"POST"},"success":false,"status":400}"""
  val succMsg=
    """{"data":{"id":"ucmiIDt","title":null,"description":null,"datetime":1546068918,
      |"type":"image\/jpeg","animated":false,"width":805,"height":1024,"size":352500,"views":0,
      |"bandwidth":0,"vote":null,"favorite":false,"nsfw":null,"section":null,"account_url":null,
      |"account_id":99426657,"is_ad":false,"in_most_viral":false,"has_sound":false,"tags":[],
      |"ad_type":0,"ad_url":"","in_gallery":false,"deletehash":"1RlVHiKJloHR7gA","name":"","link":"https:\/\/i.imgur.com\/ucmiIDt.jpg"},"success":true,"status":200}""".stripMargin

}
