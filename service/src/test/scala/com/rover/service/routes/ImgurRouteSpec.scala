package com.rover.service.routes

import com.rover.service.WebCallDAO
import com.rover.service.actors.EventService
import com.rover.service.domain.{ImageEvent, InMemoryJobDataEntityRepo}
import com.rover.service.util.TestImgurData
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.scalatest.{MustMatchers, OptionValues, WordSpec}
import play.api.libs.json.Json
import spray.http.{ContentTypes, StatusCodes}
import spray.http.HttpHeaders.`Content-Type`
import spray.testkit.ScalatestRouteTest

class ImgurRouteSpec extends WordSpec  with ScalatestRouteTest with MustMatchers with OptionValues with ScalaFutures with ImgurRoute with MockitoSugar with TestImgurData{

  val mockEventService = new EventService[ImageEvent] {
    override def publish[T <: ImageEvent](event: T): Unit = Unit
  }
  val mockExternalWebService=mock[WebCallDAO]
  val jobDataRepo=new InMemoryJobDataEntityRepo

  val routes = imgurRoute(mockEventService,mockExternalWebService,jobDataRepo)

  "ImgurRoutes" must {
    jobDataRepo.insert(jobData.copy(id="xxx"))
    jobDataRepo.insert(jobData1)
    jobDataRepo.insert(jobData2.copy(id="yyy"))
   "return all the image links form v1/images" in {
     Get("/v1/images")  ~> addHeader(`Content-Type`(ContentTypes.`application/json`)) ~> routes ~> check{
       response.status mustBe StatusCodes.OK
       (Json.parse(response.entity.data.asString) \ "uploaded").as[Set[String]] mustBe Set("https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","","","","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg","https://i.imgur.com/ucmiIDt.jpg")
     }
   }

    "return specific job details of /v1/images/upload/xxx" in{
      Get("/v1/images/upload/xxx")  ~> addHeader(`Content-Type`(ContentTypes.`application/json`)) ~> routes ~> check{
        response.status mustBe StatusCodes.OK
        (Json.parse(response.entity.data.asString) \ "status").as[String] mustBe "pending"
      }
    }

    "return job details of /v1/images/upload/yyy" in{
      Get("/v1/images/upload/yyy")  ~> addHeader(`Content-Type`(ContentTypes.`application/json`)) ~> routes ~> check{
        response.status mustBe StatusCodes.OK

        (Json.parse(response.entity.data.asString) \ "status").as[String] mustBe "completed"
        (Json.parse(response.entity.data.asString) \ "uploaded").as[Map[String,Set[String]]].get("complete").size mustBe 1
        (Json.parse(response.entity.data.asString) \ "uploaded").as[Map[String,Set[String]]].get("failed").size mustBe 1
        (Json.parse(response.entity.data.asString) \ "uploaded").as[Map[String,Set[String]]].get("pending").size mustBe 1
      }
    }

    "return job-id for /v1/images/upload" in {
      Post("/v1/images/upload/",Json.parse("""{"urls":["http://url1.jpg","http://url2.jpg"]}"""))  ~> addHeader(`Content-Type`(ContentTypes.`application/json`)) ~> routes ~> check{
        response.status mustBe StatusCodes.OK
        (Json.parse(response.entity.data.asString) \ "job-id").as[String].nonEmpty mustBe true
      }
    }
  }
}
