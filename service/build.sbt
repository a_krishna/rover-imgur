
ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

scalacOptions += "-Ylog-classpath"
val ScalaLoggingVersion= "3.1.0"
val AkkaVersion= "2.3.14"
val PlayJsonVersion= "2.3.10"
val SprayVersion= "1.3.3"
val ScalaTestVersion= "2.2.5"
val CoreVersion= "3.0.10"
val UserVersion= "2.52"
val JodaTimeVersion= "2.9.1"
val PegDown = "1.0.1"


libraryDependencies ++= Seq(
  "io.spray"                   %% "spray-routing"                         % SprayVersion          % "compile",
  "com.typesafe.play"          %% "play-json"                              % PlayJsonVersion             % Compile,
  "com.typesafe.akka"          %% "akka-actor"                            % AkkaVersion          % "compile",
  "com.typesafe.akka"          %% "akka-slf4j"                            % AkkaVersion          % "compile",
  "com.typesafe.akka"          %% "akka-http-experimental"                % "2.0-M1"             % "compile",
  "com.typesafe.akka"          %% "akka-stream-experimental"              % "2.0-M1"              % "compile",
  "com.typesafe.scala-logging" %% "scala-logging"                          % "3.1.0"         % Compile,
  "ch.qos.logback"              % "logback-classic"                       % "1.1.3"       % "compile",
  "org.scala-lang.modules"     %% "scala-async"                           % "0.9.1"              % "compile",
  "net.databinder.dispatch"    %% "dispatch-core"                         % "0.11.0"                % "compile",
  "com.typesafe"                % "config"                                % "1.2.1"                 % "compile",
  "com.typesafe.akka"          %% "akka-testkit"                          % AkkaVersion          % "test",
  "io.spray"                   %% "spray-testkit"                         % SprayVersion          % "test",
  "io.spray"                   %% "spray-can"                              % SprayVersion                % Compile,
  "io.spray"                   %% "spray-http"                             % SprayVersion                % Compile,
  "io.spray"                   %% "spray-httpx"                            % SprayVersion                % Compile,
  "io.spray"                   %% "spray-client"                           % SprayVersion                % Compile,
  "io.spray"                   %% "spray-caching"                          % SprayVersion                % Compile,
  "org.scalatest"              %% "scalatest"                             % ScalaTestVersion      % "test",
  "org.pegdown"                 % "pegdown"                               % PegDown               % "test",
  "org.easymock"                % "easymock"                              % "3.2"                   % "test",
  "org.scalacheck"              %% "scalacheck"                           % "1.12.2"                % "test",
  "org.mockito"                 % "mockito-core"                          % "1.10.19"                 % "test",
  "com.miguno.akka"             %% "akka-mock-scheduler"            % "0.4.0"         % Test
)

dependencyOverrides +=   "org.scala-lang"              % "scala-reflect"                         % "2.11.7"

testOptions in Test += sbt.Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test/service/unit", "-o")

parallelExecution in Test := false

enablePlugins(BuildInfoPlugin)
enablePlugins(DockerPlugin)


buildInfoOptions += BuildInfoOption.ToMap
buildInfoPackage := "com.rover.service"

//Assemby settings
test in assembly := {}

assemblyMergeStrategy in assembly := {
  case "application.conf" => MergeStrategy.concat
  case path => MergeStrategy.defaultMergeStrategy(path)
}

//Make assembly a publishable artifact
artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)

assemblyExcludedJars in assembly := {
  val cp = (fullClasspath in assembly).value
  cp filter {f=>
    f.data.getName == "specs2_2.11-2.3.13.jar"
  }
}

dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File = assembly.value
  val artifactTargetPath = s"/app/${artifact.name}"

  new Dockerfile {
    from("openjdk:8-jre")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-cp", artifactTargetPath,"com.rover.service.Main")
  }
}

buildOptions in docker := BuildOptions(cache = false)
