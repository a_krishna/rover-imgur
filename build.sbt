name := "rover-imgur"

organization in ThisBuild := "com.rover"


lazy val service = Project("rover-imgur-service", file("service"))

scalaVersion in ThisBuild := "2.11.7"

conflictWarning in ThisBuild := ConflictWarning.disable

parallelExecution in Test in ThisBuild := false

parallelExecution in IntegrationTest in ThisBuild := false 

scalacOptions in Compile in ThisBuild ++= Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-language:postfixOps")

logLevel in compile := Level.Error

